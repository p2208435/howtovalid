# HowToValid

[MODOT PIERRE p2208435](https://forge.univ-lyon1.fr/p2208435)  
[FONTAINE KYLIAN p2112105](https://forge.univ-lyon1.fr/p2112105)  
[DECHER JOLAN p2100764](https://forge.univ-lyon1.fr/p2100764)  
[Nom: `HowToValid` - ID: `28473`](https://forge.univ-lyon1.fr/p2208435/howtovalid)

## Description

How To Valid est un jeu de style RPG dans lequel on incarne un étudiant dans une université dévastée par d'horribles monstres.  
Les études étant malgré tout importantes, vous devrez quand même parcourir le campus et combattre les monstres pour valider votre année...  
Attention à votre note et bonne chance !

### Règles du jeu

Évitez (ou vainquez) les monstres pour atteindre votre objectif (plaque dorée) sans que votre note tombe à 0/20 pour valider votre année.

### Contrôles

| Graphique | Console | Fonction                   |
| --------- | ------- | -------------------------- |
| `Z`       | `Z`     | Déplacement vers le haut   |
| `Q`       | `Q`     | Déplacement vers la gauche |
|           |         | Cycler difficulté à gauche |
| `S`       | `S`     | Déplacement vers le bas    |
| `D`       | `D`     | Déplacement vers la droite |
|           |         | Cycler difficulté à droite |
| `O`       | `O`     | Attaque vers le haut       |
| `K`       | `K`     | Attaque vers la gauche     |
| `L`       | `K`     | Attaque vers le bas        |
| `M`       | `L`     | Attaque vers la droite     |
| `SPACE`   | `SPACE` | Utiliser un objet          |
| `LSHIFT`  | `A`     | Marcher lentement          |
| `RETURN`  | `P`     | Lancer la partie           |
| `ESCAPE`  | `!`     | Quitter                    |

## Fonctionnalités

- Plusieurs cartes à base de tuiles
  - Le jeu possède plusieurs niveaux avec chacun une carte créée à partir d'une grille de tuiles
- Carte modifiable au cours du jeu
  - Certaines actions comme marcher sur des leviers on récupérer des objets au sol modifient la carte
- Différentes tuiles à effets variés
  - Plusieurs types de tuiles sont mis à disposition tels que des tuiles blessant les entités, changeant leur vitesse, leur donnant des objets, ou modifiant la carte
- Objets utilisables
  - Des objets tels que des soins récupérables sur la cartes peuvent être utilisés pour se donner une avantage
- Différents ennemis à comportements variés
  - Plusieurs types d'ennemis possédant différentes IA sont disponibles, tels que des ennemis qui chassent le joueur et d'autres envoyant des projectiles
- Projectiles
  - Certains ennemis tirent des projectiles qui font des dégâts aux entités qu'ils touchent et cassent au moindre contact
- Animations et particules
  - Les entités sont animées et certaines actions telles que les attaques créent des particules
- Boîtes de dialogue
  - Lorsque le joueur est proche de certaines tuiles, un boîte de dialogue peut s'afficher pour lui donner des informations
- Système de configuration
  - Le système de configuration du jeu permet de facilement changer de nombreux aspects de son comportement sans avoir à le recompiler
- Interfaces graphique et textuelle
  - Le jeu possède une interface graphique ainsi qu'une interface textuelle

## Compilation et exécution

### Compilation depuis un clone du projet

```sh
git clone https://forge.univ-lyon1.fr/p2208435/howtovalid.git
cd howtovalid
make
```

### Compilation depuis une copie locale

Le `Makefile` est généré par un script et doit être regénéré après certaines actions.

#### Avec `node.js`

- `node ./make.js` permet de générer un `Makefile` qui gère les dépendances
- N'a besoin d'être regénéré que si les dépendances changent
- La configuration se trouve au début de `./make.js`
- `make [targets ...]` permet de compiler le projet

#### Avec `bash`

- `./make.sh [targets ...]` permet de générer un `Makefile` qui ne gère pas des dépendances et de compiler
- A besoin d'être regénéré si un `.h` change sans qu'un `.cpp` associé ne change
- La configuration est limitée
- `./make.sh [targets ...]` permet de générer et compiler, `make [targets ...]` permet de compiler sans regénérer

#### Targets du `Makefile`

| Target       | Fonction                             |
| ------------ | ------------------------------------ |
| `all` *      | `make_dirs` + `bin/*`                |
| `nothing`    | ne fait rien                         |
| `docs`       | construit la doc avec `doxygen`      |
| `format`     | formatte le code avec `clang-format` |
| `clean`      | supprime les fichiers générés        |
| `make_dirs`  | crée les dossiers nécéssaires        |
| `bin/*`      | lie les binaires                     |
| `obj/**/*.o` | compile les objets                   |

\* Target par défaut

### Exécution

Les binaires doivent être exécutés depuis la racine du projet (cherchent des données dans `./data/`).  
Version graphique: `bin/HowToValid`  
Version textuelle: `bin/HowToValid_console`  
Tests d'intégrité: `bin/HowToValid_tests`

## Organisation de l'archive

| Chemin          | Contenu                                |
| --------------- | -------------------------------------- |
| `src/*/`        | sources C++                            |
| `src/sdl/`      | interface graphique (contient un main) |
| `src/txt/`      | interface textuelle (contient un main) |
| `src/test/`     | tests d'intégrité (contient un main)   |
| `obj/`          | fichiers objets (issus de compilation) |
| `bin/`          | binaires (issus de compilation)        |
| `doc/Doxyfile`  | configuration de `doxygen`             |
| `doc/html/`     | documentation au format HTML           |
| `data/`         | données                                |
| `tmp/`          | fichiers temporaires                   |
| `make.js`       | générateur de `Makefile` (`nodejs`)    |
| `make.sh`       | générateur de `Makefile` (`bash`)      |
| `Makefile`      | processus de compilation (`make`)      |
| `.clang-format` | configuration de `clang-format`        |

## Formats de données

### Configuration

Liste Type-Clé-Valeur

| Id  | Type     | Informations              |
| --- | -------- | ------------------------- |
| `p` | Chemin   | Espaces de début tronqués |
|     |          | Terminé par LF/CR/EOF     |
|     |          | Racine: dossier `data`    |
| `t` | Texte    | Espaces de début tronqués |
|     |          | Terminé par LF/CR/EOF     |
|     |          | `\` -> fin de ligne       |
| `s` | Chaîne   | Délimité par `"`          |
|     |          | Séquences d'échappement C |
|     |          | Pas de concaténation      |
|     |          | EOF -> indéfini           |
| `i` | Entier   | Décimal                   |
| `f` | Flottant | Décimal                   |

### Format des niveaux

- `int`: nombre de leviers présents sur la carte
- liste des leviers:
  - `int int`: position (x, y) du levier
  - `int`: nombre de tuiles changées
  - liste des tuiles changées:
    - `int int`: position (x, y) de la tuile
    - `char`: type de la tuile
    - `int`: texture de la tuile
- `int`: nombre d'objets présents sur la carte
- liste des objets:
  - `int int`: position (x, y) de l'objet
  - `char`: type du sol
  - `int`: texture du sol
- `int int`: taille (w, h) de la carte
- liste des tuiles constituant la carte:
  - `char`: type de la tuile
  - `int`: texture de la tuile
- `int int`: position (x, y) du joueur
- `int`: nombre d'ennemis présents sur la carte
- liste des ennemis:
  - `int`: type de l'ennemi
  - `int int`: position (x, y) de l'ennemi
  - `int`: vie de l'ennemi
- `int`: nombre de dialogues présents sur la carte
- liste des dialogues:
  - `int int`: position du dialogue
  - `texte`: texte du dialogue (c.f. Configuration > Texte)

#### Types d'ennemis

| Numéro | Type    | Distance de suivi | Dégâts | Informations                 |
| ------ | ------- | ----------------- | ------ | ---------------------------- |
| `0`    | Normal  | 4                 | 2      |                              |
| `1`    | Malin   | 3                 | 1      | Ne marche pas sur `/` et `*` |
| `2`    | Lanceur | 7                 | 2      | Lance des projectiles        |

#### Types de tuiles

| Symbole | Type         | Hauteur |
| ------- | ------------ | ------- |
| `.`     | Normale      | 0       |
| `;`     | Normale      | 1       |
| `#`     | Normale      | 2       |
| `!`     | Blessante    | 0       |
| `*`     | Objectif     | 0       |
| `~`     | Ralentissant | 0       |
| `+`     | Objet:Soin   | 0       |
| `/`     | Levier       | 0       |

### Format des textures de tuiles

Tuiles de 32x32  
Grille de `w`x`h` tuiles  
`indice = x + y * w`  

### Format des textures d'entités

Tuiles de 32x32  
Contient `k` variantes  
Grille de 6x3`k` tuiles  
`indice = x + y * 6`
```
droite_normal1 droite_normal2 droite_marche_ bas____normal1 bas____normal2 bas____marche_
gauche_normal1 gauche_normal2 gauche_marche_ haut___normal1 haut___normal2 haut___marche_
droite_attaque bas____attaque gauche_attaque haut___attaque ______________ ______________
```
