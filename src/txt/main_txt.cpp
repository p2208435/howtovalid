#include "../partie/Config.h"
#include "../partie/Partie.h"
#include "txtJeu.h"
#include "winTxt.h"

int main(int argc, char** argv) {
	termClear();
	Config config("./data/data.txt");
	Partie partie(&config);
	TxtJeu win(&partie, &config, 20, 14, 2,
	           ".!~;;;/..!~;;;/."
	           ".#;;++**#######."
	           "################"
	           "########........"
	           "########....;;;;"
	           "P---S//......???",
	           "JJJJJJ"
	           "JJJJJJ"
	           ">v<^--"
	           "JJJJJJ"
	           "JJJJJJ"
	           ">v<^--"
	           "JJJJJJ"
	           "JJJJJJ"
	           ">v<^--",
	           "EEEEEE"
	           "EEEEEE"
	           ">v<^--"
	           "MMMMMM"
	           "MMMMMM"
	           ">v<^--"
	           "LLLLLL"
	           "LLLLLL"
	           ">v<^--");
	win.boucle(100);
	termClear();
	return 0;
}
