#ifndef _TXTJEU_H
#define _TXTJEU_H

#include "../partie/Config.h"
#include "../partie/Partie.h"
#include "winTxt.h"
#include <string>
#include <vector>

/**
* @brief Un environnement textuel pour l'affichage d'une partie.
*/
class TxtJeu {
public:
	/**
	 * @brief Construit un environnement textuel pour l'affichage d'une partie.
	 * @param partie La partie à afficher.
	 * @param config La configuration de l'affichage.
	 * @param maxW La largeur maximale.
	 * @param maxH La heuteur maximale.
	 * @param zoom Le niveau de zoom.
	 * @param texTuiles Les textures des tuiles.
	 * @param texJoueur La texture du joueur.
	 * @param texEnnemis Les textures des ennemis.
	 */
	TxtJeu(Partie* partie, const Config* config, int maxW, int maxH, int zoom,
	       const char* texTuiles, const char* texJoueur,
	       const char* texEnnemis);

	/**
	 * @brief Détruit l'environnement textuel.
	 */
	~TxtJeu();

	/**
	 * @brief Lance la boucle de jeu dans l'environnement textuel.
	 * @param mspt Le nombre de millisecondes pris par un tour de boucle.
	 */
	void boucle(unsigned int mspt);

private:
	/**
	 * @brief La partie gérée par l'environnement textuel.
	 */
	Partie* partie;

	/**
	 * @brief La configuration de l'environnement textuel.
	 */
	const Config* config;

	/**
	 * @brief La largeur maximale de la carte.
	 */
	int maxW;

	/**
	 * @brief La hauteur maximale de la carte.
	 */
	int maxH;

	/**
	 * @brief Le niveau de zoom.
	 */
	int zoom;

	/**
	 * @brief Les textures des tuiles.
	 */
	const char* texTuiles;

	/**
	 * @brief Les textures du joueur.
	 */
	const char* texJoueur;

	/**
	 * @brief Les textures des ennemis.
	 */
	const char* texEnnemis;

	/**
	 * @brief La fenêtre ouverte par l'environnement.
	 */
	WinTXT win;

	// Affichage des différents écrans

	/**
	 * @brief Affiche l'écran titre.
	 */
	void afficheEcranTitre();

	/**
	 * @brief Affiche l'écran de victoire.
	 */
	void afficheEcranVictoire();

	/**
	 * @brief Affiche l'écran de transition.
	 */
	void afficheEcranSuivant();

	/**
	 * @brief Affiche l'écran de défaite.
	 */
	void afficheEcranDefaite();

	/**
	 * @brief Affiche la carte.
	 */
	void afficheCarte();

	/**
	 * @brief Affiche l'interface.
	 */
	void afficheInterface();

	// Affichage du texte

	/**
	 * @brief Affiche un pixel.
	 * @param x La position X.
	 * @param y La position Y.
	 * @param texture La texture.
	 */
	void affichePixel(int x, int y, char texture);

	/**
	 * @brief Affiche du texte.
	 * @param x La position X.
	 * @param y La position Y.
	 * @param texte Le texte.
	 */
	void afficheTexte(int x, int y, const std::vector<std::string>& texte);
};

#endif
