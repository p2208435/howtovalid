#include "TuileRalentissant.h"

TuileRalentissant::TuileRalentissant(double vitesse) : Tuile(0) {
	this->malusVitesse = vitesse;
};

TuileRalentissant::~TuileRalentissant() {}

void TuileRalentissant::subirTuile(Entite& entite, unsigned int x,
                                   unsigned int y,
                                   std::vector<ChangementTuile>& changements) {
	entite.changerVitesse(entite.getVitesse() * this->malusVitesse);
}
