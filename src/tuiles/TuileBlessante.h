#ifndef _TUILE_BLESSANTE_H
#define _TUILE_BLESSANTE_H

#include "Tuile.h"

/**
 * @brief Une tuile infligeant des dégâts.
 */
class TuileBlessante : public Tuile {
public:
	/**
	 * @brief Construit une tuile blessante.
	 * @param valeurDegats Les dégâts infligés.
	 */
	TuileBlessante(int valeurDegats = 1);

	/**
	 * @brief Détruit la tuile blessante.
	 */
	~TuileBlessante();

	/**
	 * @brief Blesse une entité.
	 * @param entite L'entité.
	 * @param x La position x de la tuile.
	 * @param y La position y de la tuile.
	 * @param changements Le `vector` dans lequel les changements de tuiles effectués seront écrits.
	 */
	void subirTuile(Entite& entite, unsigned int x, unsigned int y,
	                std::vector<ChangementTuile>& changements);

private:
	/**
	 * @brief Les dégâts infligés par la tuile.
	 */
	int degats;
};

#endif
