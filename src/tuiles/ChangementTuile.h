#ifndef _CHANGEMENT_TUILE_H
#define _CHANGEMENT_TUILE_H

/**
 * @brief Un struct représentant un changement de tuile.
 */
struct ChangementTuile {
	/**
	 * @brief La position X de la tuile.
	 */
	unsigned int x;

	/**
	 * @brief La position Y de la tuile.
	 */
	unsigned int y;

	/**
	 * @brief Le type de la tuile.
	 */
	char type;

	/**
	 * @brief La texture de la tuile.
	 */
	unsigned int texture;
};

#endif
