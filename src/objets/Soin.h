#ifndef _SOIN_H
#define _SOIN_H

#include "Objet.h"

/**
 * @brief Un objet de soin.
 */
class Soin : public Objet {
public:
	/**
	 * @brief Construit un soin.
	 * @param pvBonus Le nombre de pv restaurés à l'utilisation.
	 */
	Soin(unsigned int pvBonus);

	/**
	 * @brief Détruit le soin.
	 */
	~Soin();

	/**
	 * @brief Utilise le soin sur une entité.
	 */
	bool utiliser(Entite& entite) const;

private:
	/**
	 * @brief Nombre de pv rendus lors de l'utilisation.
	 */
	unsigned int pvBonus;
};

#endif
