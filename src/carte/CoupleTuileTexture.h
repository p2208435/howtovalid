#ifndef _COUPLE_TUILE_TEXTURE_H
#define _COUPLE_TUILE_TEXTURE_H

#include "../tuiles/Tuile.h"

/**
 * @brief Un struct représentant un couple d'une Tuile et de l'indice de sa texture.
 */
struct CoupleTuileTexture {
	/**
	 * @brief La tuile.
	 */
	Tuile* tuile;

	/**
	 * @brief La texture.
	 */
	unsigned int texture;
};

#endif
