#ifndef _CARTE_H
#define _CARTE_H

#include "../tuiles/Tuile.h"
#include "CoupleTuileTexture.h"
#include <istream>
#include <unordered_map>
#include <vector>

/**
 * @brief Une carte du monde, contenant des tuiles avec les indices de leurs textures.
 */
class Carte {
public:
	/**
	 * @brief Construit une Carte.
	 * @param data Les données de la carte.
	 */
	Carte(std::istream& data);

	/**
	 * @brief Détruit la Carte.
	 */
	~Carte();

	/**
	 * @brief Retourne la largeur de la carte.
	 * @return La largeur de la carte.
	 */
	unsigned int getLargeur() const;

	/**
	 * @brief Retourne la hauteur de la carte.
	 * @return La hauteur de la carte.
	 */
	unsigned int getHauteur() const;

	/**
	 * @brief Recupère une tuile.
	 * @param x La position X.
	 * @param y La position Y.
	 * @return La tuile en position `x`,`y`.
	 */
	Tuile* getTuile(unsigned int x, unsigned int y) const;

	/**
	 * @brief Récupère un type de tuile.
	 * @param x La position X.
	 * @param y La position Y.
	 * @return Le type de tuile en position `x`,`y` (ex. '.', '#', ...).
	 */
	char getType(unsigned int x, unsigned int y) const;

	/**
	 * @brief Récupère une texture.
	 * @param x La position X.
	 * @param y La position Y.
	 * @return L'indice de la tuile en position `x`,`y`.
	 */
	unsigned int getTexture(unsigned int x, unsigned int y) const;

	/**
	 * @brief Pose une tuile.
	 * @param x La position X.
	 * @param y La position Y.
	 * @param tuile La tuile.
	 */
	void setTuile(unsigned int x, unsigned int y, Tuile* tuile);

	/**
	 * @brief Pose un type de tuile.
	 * @param x La position X.
	 * @param y La position Y.
	 * @param type Le type de tuile (ex. '.', '#', ...).
	 */
	void setType(unsigned int x, unsigned int y, char type);

	/**
	 * @brief Pose une texture.
	 * @param x La position X.
	 * @param y La position Y.
	 * @param texture L'indice de la texture.
	 */
	void setTexture(unsigned int x, unsigned int y, unsigned int texture);

	/**
	 * @brief Affiche le contenu de la carte (debug).
	 */
	void affichageDebug() const;

private:
	/**
	 * @brief Largeur de la carte.
	 */
	unsigned int largeur;

	/**
	 * @brief Hauteur de la carte.
	 */
	unsigned int hauteur;

	/**
	 * @brief Tableau des tuiles et textures.
	 */
	std::vector<CoupleTuileTexture> carte;

	/**
	 * @brief Correspondance caractère tuile.
	 */
	std::unordered_map<char, Tuile*> tuiles;
};

#endif
