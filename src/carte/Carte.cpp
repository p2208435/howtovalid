#include "Carte.h"
#include "../objets/Soin.h"
#include "../tuiles/TuileBlessante.h"
#include "../tuiles/TuileLevier.h"
#include "../tuiles/TuileNormale.h"
#include "../tuiles/TuileObjectif.h"
#include "../tuiles/TuileObjet.h"
#include "../tuiles/TuileRalentissant.h"
#include <iostream>

Carte::Carte(std::istream& data) {
	// Initialisations des leviers
	std::vector<std::vector<unsigned int>> leviers;
	unsigned int nbLeviers;
	data >> nbLeviers;
	for (unsigned int i = 0; i < nbLeviers; i++) {
		std::vector<unsigned int> ligne;
		unsigned int x, y, nbChg;
		data >> x >> y >> nbChg;
		ligne.push_back(x);
		ligne.push_back(y);
		for (unsigned int j = 0; j < nbChg; j++) {
			unsigned int nx, ny, nTex;
			char nType;
			data >> nx >> ny >> nType >> nTex;
			ligne.push_back(nx);
			ligne.push_back(ny);
			ligne.push_back((unsigned char)nType);
			ligne.push_back(nTex);
		}
		ligne.shrink_to_fit();
		leviers.push_back(ligne);
	}
	leviers.shrink_to_fit();

	// Initialisation des objets
	std::vector<ChangementTuile> objets;
	unsigned int nbObjets;
	data >> nbObjets;
	for (unsigned int i = 0; i < nbObjets; i++) {
		ChangementTuile chg;
		data >> chg.x >> chg.y >> chg.type >> chg.texture;
		objets.push_back(chg);
	}
	objets.shrink_to_fit();

	// Initialisation de la carte
	data >> this->largeur >> this->hauteur;
	this->carte.resize(this->largeur * this->hauteur);

	// Stockage d'une tuile par type
	this->tuiles.clear();
	this->tuiles.insert_or_assign('.', new TuileNormale(0));
	this->tuiles.insert_or_assign(';', new TuileNormale(1));
	this->tuiles.insert_or_assign('#', new TuileNormale(2));
	this->tuiles.insert_or_assign('!', new TuileBlessante());
	this->tuiles.insert_or_assign('*', new TuileObjectif());
	this->tuiles.insert_or_assign('~', new TuileRalentissant(0.5));
	this->tuiles.insert_or_assign('+', new TuileObjet(new Soin(5), objets));
	this->tuiles.insert_or_assign('/', new TuileLevier(leviers));

	// Lecture des tuiles
	for (unsigned int y = 0; y < this->hauteur; y++) {
		for (unsigned int x = 0; x < this->largeur; x++) {
			char type;
			data >> type;
			this->carte[x + this->largeur * y].tuile = this->tuiles.at(type);
			data >> this->carte[x + this->largeur * y].texture;
		}
	}
}

Carte::~Carte() {
	for (const auto& [_, tuile] : this->tuiles) {
		delete tuile;
	}
}

unsigned int Carte::getLargeur() const { return this->largeur; }

unsigned int Carte::getHauteur() const { return this->hauteur; }

Tuile* Carte::getTuile(unsigned int x, unsigned int y) const {
	return this->carte.at(x + this->largeur * y).tuile;
}

char Carte::getType(unsigned int x, unsigned int y) const {
	Tuile* ref = this->carte.at(x + this->largeur * y).tuile;

	// Recherche du type de la tuile
	for (const auto& [type, tuile] : this->tuiles) {
		if (tuile == ref)
			return type;
	}
	return '\0';
}

unsigned int Carte::getTexture(unsigned int x, unsigned int y) const {
	return this->carte.at(x + this->largeur * y).texture;
}

void Carte::setTuile(unsigned int x, unsigned int y, Tuile* tuile) {
	this->carte.at(x + this->largeur * y).tuile = tuile;
}

void Carte::setType(unsigned int x, unsigned int y, char type) {
	this->carte.at(x + this->largeur * y).tuile = this->tuiles.at(type);
}

void Carte::setTexture(unsigned int x, unsigned int y, unsigned int texture) {
	this->carte.at(x + this->largeur * y).texture = texture;
}

void Carte::affichageDebug() const {
	// Affichage correspondance (nom du type) : (pointeur de la tuile)
	for (const auto& [id, tuile] : this->tuiles) {
		std::cout << id << ":" << tuile << " ";
	}
	std::cout << std::endl;

	// Affichage tableau (pointeur) = (type) : (texture)
	for (unsigned int y = 0; y < this->getHauteur(); y++) {
		for (unsigned int x = 0; x < this->getLargeur(); x++) {
			std::cout << this->getTuile(x, y) << "=" << this->getType(x, y)
					  << ":" << this->getTexture(x, y) << " ";
		}
		std::cout << std::endl;
	}
}
