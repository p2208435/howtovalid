#ifndef _DIALOGUE_H
#define _DIALOGUE_H

#include <string>
#include <vector>

/**
 * @brief Un struct représentant un dialogue avec sa position.
 */
struct Dialogue {
	/**
	 * @brief La position X du dialogue.
	 */
	unsigned int x;

	/**
	 * @brief La position Y du dialogue.
	 */
	unsigned int y;

	/**
	 * @brief Le texte du dialogue.
	 */
	std::vector<std::string> texte;
};

#endif
