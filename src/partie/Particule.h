#ifndef _PARTICULE_H
#define _PARTICULE_H

/**
 * @brief Une particule.
 */
class Particule {
public:
	/**
	 * @brief Construit une particule.
	 * @param x Sa position x.
	 * @param y Sa position y.
	 * @param angle Son angle.
	 * @param texture Sa texture.
	 * @param tempsRestant Sa durée de vie.
	 */
	Particule(double x, double y, double angle, int texture,
	          unsigned int tempsRestant);

	/**
	 * @brief Construit une particule.
	 * @param x Sa position x de départ.
	 * @param y Sa position y de départ.
	 * @param angle Son angle de départ.
	 * @param texture Sa texture de départ.
	 * @param vx Son changement de position x.
	 * @param vy Son changement de position y.
	 * @param vangle Son changement d'angle.
	 * @param vtexture Son changement de texture.
	 * @param tempsRestant Sa durée de vie.
	 */
	Particule(double x, double y, double angle, int texture, double vx,
	          double vy, double vangle, int vtexture,
	          unsigned int tempsRestant);

	/**
	 * @brief Avance l'état de la particule.
	 */
	void tic();

	/**
	 * @brief Retouene la position x de la particule.
	 * @return La position x de la particule.
	 */
	double getX() const;

	/**
	 * @brief Retourne la position y de le particule.
	 * @return La position y de la particule.
	 */
	double getY() const;

	/**
	 * @brief Retourne l'angle de la particule.
	 * @return L'angle de la particule.
	 */
	double getAngle() const;

	/**
	 * @brief Retourne la texture de la particule.
	 * @return La texture de la particule.
	 */
	int getTexture() const;

	/**
	 * @brief Retourne la durée de vie de la particule.
	 * @return La durée de vie de la particule.
	 */
	unsigned int getTempsRestant() const;

private:
	/**
	 * @brief La position X de la particule.
	 */
	double x;

	/**
	 * @brief La position Y de la particule.
	 */
	double y;

	/**
	 * @brief La vitesse X de la particule.
	 */
	double vx;

	/**
	 * @brief La vitesse Y de la particule.
	 */
	double vy;

	/**
	 * @brief L'angle de la particule.
	 */
	double angle;

	/**
	 * @brief La vitesse angulaire de la particule.
	 */
	double vangle;

	/**
	 * @brief La texture de la particule.
	 */
	int texture;

	/**
	 * @brief Le changement de texture de la particule.
	 */
	int vtexture;

	/**
	 * @brief La durée de vie de la particule.
	 */
	unsigned int tempsRestant;
};

#endif
