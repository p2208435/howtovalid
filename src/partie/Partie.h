#ifndef _PARTIE_H
#define _PARTIE_H

#include "../carte/Carte.h"
#include "../entites/Ennemi.h"
#include "../entites/Entite.h"
#include "../entites/Joueur.h"
#include "../entites/Projectile.h"
#include "Config.h"
#include "Dialogue.h"
#include "Particule.h"
#include <string>
#include <vector>

/**
 * @brief Une instance d'une partie.
 */
class Partie {
public:
	// Construction et destruction

	/**
	 * @brief Construit une partie.
	 * @param config La configuration de la partie.
	 */
	Partie(Config* config);

	/**
	 * @brief Détruit la partie.
	 */
	~Partie();

	// Getters

	/**
	 * @brief Retourne une référence constante vers la carte.
	 * @return Une référence constante vers la carte.
	 */
	const Carte& getConstCarte() const;

	/**
	 * @brief Retourne une référence constante vers le joueur.
	 * @return Une référence contante vers le joueur.
	 */
	const Joueur& getConstJoueur() const;

	/**
	 * @brief Retourne une référence constante vers la liste des ennemis.
	 * @return Une référence constante vers la liste des ennemis.
	 */
	const std::vector<Ennemi*>& getConstEnnemis() const;

	/**
	 * @brief Retourne une référence constante vers la liste des projectiles.
	 * @return Une référence constante vers la liste des projectiles.
	 */
	const std::vector<Projectile*>& getConstProjectiles() const;

	/**
	 * @brief Retourne une référence constante vers la liste des dialogues.
	 * @return Une référence constante vers la liste des dialogues.
	 */
	const std::vector<Dialogue>& getConstDialogues() const;

	/**
	 * @brief Retourne une référence constante vers la liste des particules.
	 * @return Une référence constante vers la liste des particules.
	 */
	const std::vector<Particule*>& getConstParticules() const;

	/**
	 * @brief Retourne si la partie est lancée (en jeu, victoire, entre deux niveaux, défaite).
	 * @return Un booléen indiquant si la partie est lancée.
	 */
	bool getPartieEnCours() const;

	/**
	 * @brief Retourne la difficulte.
	 * @return La difficulté de la partie en cours.
	 */
	int getDifficulte() const;

	/**
	 * @brief Vérifie si la partie est terminée (victoire, entre deux niveaux, défaite).
	 * @return Vrai si la partie est terminée, faux sinon.
	 */
	bool fini();

	/**
	 * @brief Retourne l'indice du niveau courant.
	 * @return L'indice du niveau courant.
	 */
	unsigned int getNiveau() const;

	/**
	 * @brief Vérifie si la partie est gagnée (victoire de tous les niveaux).
	 * @return Vrai si la partie est gagnée, faux sinon.
	 */
	bool gagne() const;

	// Actions de la boucle de jeu

	/**
	 * @brief Effectue les actions qui devront être executées à chaque tour de boucle.
	 */
	void actionsAutomatiques();

	/**
	 * @brief Effectue une action en fonction de la touche passée en paramètre.
	 * @param touche L'identifiant de la touche.
	 */
	void actionClavier(char touche);

private:
	// État de la partie

	/**
	 * @brief La configuration de l'environnement graphique.
	 */
	Config* config;

	/**
	 * @brief La difficulté de la partie.
	 */
	int difficulte;

	/**
	 * @brief La liste des noms des cartes à jouer pour terminer le jeu.
	 */
	std::vector<std::string> cartes;

	/**
	 * @brief L'indice du niveau actuel.
	 */
	unsigned int niveau;

	/**
	 * @brief Si la partie est en lancée (en jeu, victoire, entre deux niveaux, défaite) ou non.
	 */
	bool partieEnCours;

	/**
	 * @brief La carte actuelle.
	 */
	Carte* carte;

	/**
	 * @brief Le joueur actuel.
	 */
	Joueur* joueur;

	/**
	 * @brief La liste des ennemis en vie.
	 */
	std::vector<Ennemi*> ennemis;

	/**
	 * @brief La liste des projectiles en vie.
	 */
	std::vector<Projectile*> projectiles;

	/**
	 * @brief La liste des dialogues.
	 */
	std::vector<Dialogue> dialogues;

	/**
	 * @brief La liste des particules en vie.
	 */
	std::vector<Particule*> particules;

	// Remise à zéro

	/**
	 * @brief Détruit un portion de l'état de la partie.
	 */
	void viderPartie();

	/**
	 * @brief Remet à zéro une portion de l'état de la partie.
	 * @param carte Le nom de la carte à charger.
	 */
	void resetPartie(const std::string& carte);

	/**
	 * @brief Applique les effets d'un changement de difficulté.
	 */
	void appliquerDiff();

	// Actions du jeu

	/**
	 * @brief Effectue les actions des ennemis qui devront être executées à chaque tour de boucle.
	 */
	void actionsEnnemis();

	/**
	 * @brief Effectue les actions des projectiles qui devront être executées à chaque tour de boucle.
	 */
	void actionsProjectiles();

	/**
	 * @brief Avance l'état des animations d'une entité.
	 * @param entite L'entité.
	 */
	void animationAuto(Entite& entite);

	// Calcul de déplacement maximal

	/**
	 * @brief Calcule le plus grand déplacement possible vers la gauche pour l'entité en paramètre.
	 * @param entite Entite qui essaye de se déplacer.
	 * @return Plus grand déplacement possible vers la gauche.
	 */
	double maxDeplacementGauche(const Entite& entite) const;

	/**
	 * @brief Calcule le plus grand déplacement possible vers la droite pour l'entité en paramètre.
	 * @param entite Entite qui essaye de se déplacer.
	 * @return Plus grand déplacement possible vers la droite.
	 */
	double maxDeplacementDroite(const Entite& entite) const;

	/**
	 * @brief Calcule le plus grand déplacement possible vers le bas pour l'entité en paramètre.
	 * @param entite Entite qui essaye de se déplacer.
	 * @return Plus grand déplacement possible vers le bas.
	 */
	double maxDeplacementHaut(const Entite& entite) const;

	/**
	 * @brief Calcule le plus grand déplacement possible vers le bas pour l'entité en paramètre.
	 * @param entite Entite qui essaye de se déplacer.
	 * @return Plus grand déplacement possible vers le bas.
	 */
	double maxDeplacementBas(const Entite& entite) const;

	// Calculs d'entités dans des boîtes

	/**
	 * @brief Calcule les indices des ennemis présents dans une boîte.
	 * @param minX Le X minimal (inclusif).
	 * @param maxX Le X maximal (inclusif).
	 * @param minY Le Y minimal (inclusif).
	 * @param maxY Le Y maximal (inclusif).
	 * @param indices La liste à laquelle sera rajoutée les indices des ennemis trouvés.
	 */
	void getIndicesEnnemisDansBoite(double minX, double maxX, double minY,
	                                double maxY,
	                                std::vector<unsigned int>& indices) const;

	/**
	 * @brief Calcule si le joueur se trouve dans une boîte.
	 * @param minX Le X minimal (inclusif).
	 * @param maxX Le X maximal (inclusif).
	 * @param minY Le Y minimal (inclusif).
	 * @param maxY Le Y maximal (inclusif).
	 * @return Vrai si le joueur est dans la boîte, faux sinon.
	 */
	bool joueurEstDansBoite(double minX, double maxX, double minY,
	                        double maxY) const;

	// Constantes

	/**
	 * @brief La taille minimale d'un pas pour les calculs de distance.
	 */
	constexpr static const double MIN_STEP = 0.5;

	/**
	 * @brief La précision maximale pour les calculs de boîte exclusifs.
	 */
	constexpr static const double MAX_PREC = 0.99;
};

#endif
