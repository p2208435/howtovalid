#include "Config.h"
#include <iostream>

Config::Config(std::string path) : Config(path.c_str()) {}

Config::Config(const char* path) {
	std::ifstream file(path);
	this->paths.clear();
	this->texts.clear();
	this->strings.clear();
	this->ints.clear();
	while (!file.eof()) {
		char type;
		std::string key;
		file >> type >> key;
		if (file.eof() || file.peek() == -1)
			break;
		switch (type) {
		case 'p': {
			// path
			std::string value;
			value.clear();
			Config::parsePath(file, value);
			value.shrink_to_fit();
			this->paths.insert_or_assign(key, value);
		} break;
		case 't': {
			// text
			std::vector<std::string> value;
			value.clear();
			Config::parseText(file, value);
			value.shrink_to_fit();
			this->texts.insert_or_assign(key, value);
		} break;
		case 's': {
			// string
			std::string value;
			value.clear();
			Config::parseString(file, value);
			value.shrink_to_fit();
			this->strings.insert_or_assign(key, value);
		} break;
		case 'i': {
			// int
			int value;
			file >> value;
			this->ints.insert_or_assign(key, value);
		} break;
		case 'f': {
			// double
			double value;
			file >> value;
			this->doubles.insert_or_assign(key, value);
		} break;
		default: {
			std::cerr << "Type `" << type << "` (" << ((int)(unsigned char)type)
					  << ") invalide" << std::endl;
			exit(1);
		}
		}
	}
}

const std::string& Config::getPath(const std::string& key) const {
	if (auto search = this->paths.find(key); search != this->paths.end()) {
		return search->second;
	} else {
		std::cerr << "Path `" << key << "` n'existe pas" << std::endl;
		exit(1);
	}
}

const std::string& Config::getPath(const char* key) const {
	return this->getPath(std::string(key));
}

const std::vector<std::string>& Config::getText(const std::string& key) const {
	if (auto search = this->texts.find(key); search != this->texts.end()) {
		return search->second;
	} else {
		std::cerr << "Text `" << key << "` n'existe pas" << std::endl;
		exit(1);
	}
}

const std::vector<std::string>& Config::getText(const char* key) const {
	return this->getText(std::string(key));
}

const std::string& Config::getString(const std::string& key) const {
	if (auto search = this->strings.find(key); search != this->strings.end()) {
		return search->second;
	} else {
		std::cerr << "String `" << key << "` n'existe pas" << std::endl;
		exit(1);
	}
}

const std::string& Config::getString(const char* key) const {
	return this->getString(std::string(key));
}

int Config::getInt(const std::string& key) const {
	if (auto search = this->ints.find(key); search != this->ints.end()) {
		return search->second;
	} else {
		std::cerr << "Int `" << key << "` n'existe pas" << std::endl;
		exit(1);
	}
}

int Config::getInt(const char* key) const {
	return this->getInt(std::string(key));
}

double Config::getDouble(const std::string& key) const {
	if (auto search = this->doubles.find(key); search != this->doubles.end()) {
		return search->second;
	} else {
		std::cerr << "Float `" << key << "` n'existe pas" << std::endl;
		exit(1);
	}
}

double Config::getDouble(const char* key) const {
	return this->getDouble(std::string(key));
}

void Config::logContents() const {
	printf("\e[31mPaths\e[0m\n");
	for (const auto& [key, value] : this->paths) {
		printf("\e[32m[\e[0m%s\e[32m] \e[34m[\e[0m%s\e[34m]\e[0m\n",
		       key.c_str(), value.c_str());
	}
	printf("\e[31mTexts\e[0m\n");
	for (const auto& [key, value] : this->texts) {
		printf("\e[32m[\e[0m%s\e[32m]\e[0m\n", key.c_str());
		for (unsigned int i = 0; i < value.size(); i++) {
			printf("\e[34m- [\e[0m%s\e[34m]\e[0m\n", value[i].c_str());
		}
	}
	printf("\e[31mStrings\e[0m\n");
	for (const auto& [key, value] : this->strings) {
		printf("\e[32m[\e[0m%s\e[32m] \e[34m[\e[0m%s\e[34m]\e[0m\n",
		       key.c_str(), value.c_str());
	}
	printf("\e[31mInts\e[0m\n");
	for (const auto& [key, value] : this->ints) {
		printf("\e[32m[\e[0m%s\e[32m] \e[34m[\e[0m%d\e[34m]\e[0m\n",
		       key.c_str(), value);
	}
	printf("\e[31mFloats\e[0m\n");
	for (const auto& [key, value] : this->doubles) {
		printf("\e[32m[\e[0m%s\e[32m] \e[34m[\e[0m%f\e[34m]\e[0m\n",
		       key.c_str(), value);
	}
}

int Config::parseHex1(char hex) {
	if (hex >= '0' && hex <= '9')
		return hex - '0';
	if (hex >= 'A' && hex <= 'F')
		return hex - 'A' + 10;
	if (hex >= 'a' && hex <= 'f')
		return hex - 'a' + 10;
	std::cerr << "Caractère `" << hex << "` (" << ((int)(unsigned char)hex)
			  << ") n'est pas un chiffre hexadécimal" << std::endl;
	exit(1);
}

inline char Config::parseHex2(char left, char right) {
	return (Config::parseHex1(left) << 4) | Config::parseHex1(right);
}

void Config::parsePath(std::ifstream& input, std::string& output) {
	output.append("./data/");
	char next = ' ';
	while (!input.eof() && next == ' ')
		input.get(next);
	while (!input.eof() && next != '\r' && next != '\n') {
		output.push_back(next);
		input.get(next);
	}
}

void Config::parseText(std::ifstream& input, std::vector<std::string>& output) {
	std::string line;
	line.clear();
	char next = ' ';
	while (!input.eof() && next == ' ')
		input.get(next);
	while (!input.eof() && next != '\r' && next != '\n') {
		if (next == '\\') {
			line.shrink_to_fit();
			output.push_back(line);
			line = std::string();
			line.clear();
		} else
			line.push_back(next);
		input.get(next);
	}
	line.shrink_to_fit();
	output.push_back(line);
}

void Config::parseString(std::ifstream& input, std::string& output) {
	char next = ' ';
	while (!input.eof() && next == ' ')
		input.get(next);
	if (next != '"') {
		std::cerr << "Délimiteur de chaîne de caractères `" << next << "` ("
				  << ((int)(unsigned char)next) << ") invalide" << std::endl;
		exit(1);
	}
	input.get(next);
	while (!input.eof() && next != '"') {
		if (next == '\\') {
			input.get(next);
			switch (next) {
			case 'a': {
				output.push_back('\a');
				input.get(next);
			} break;
			case 'b': {
				output.push_back('\b');
				input.get(next);
			} break;
			case 'e': {
				output.push_back('\e');
				input.get(next);
			} break;
			case 'f': {
				output.push_back('\f');
				input.get(next);
			} break;
			case 'n': {
				output.push_back('\n');
				input.get(next);
			} break;
			case 'r': {
				output.push_back('\r');
				input.get(next);
			} break;
			case 't': {
				output.push_back('\t');
				input.get(next);
			} break;
			case 'v': {
				output.push_back('\v');
				input.get(next);
			} break;
			case '\\': {
				output.push_back('\\');
				input.get(next);
			} break;
			case '\'': {
				output.push_back('\'');
				input.get(next);
			} break;
			case '"': {
				output.push_back('"');
				input.get(next);
			} break;
			case '?': {
				output.push_back('?');
				input.get(next);
			} break;
			case 'x': {
				int result = 0;
				input.get(next);
				while ((next >= '0' && next <= '7') ||
				       (next >= 'a' && next <= 'b') ||
				       (next >= 'A' && next <= 'B')) {
					result = (result << 4) | Config::parseHex1(next);
					input.get(next);
				}
				output.push_back(result);
			} break;
			case 'u': {
				char next2;
				for (int i = 0; i < 2; i++) {
					input.get(next2);
					output.push_back(Config::parseHex2(next, next2));
					input.get(next);
				}
			} break;
			case 'U': {
				char next2;
				for (int i = 0; i < 4; i++) {
					input.get(next2);
					output.push_back(Config::parseHex2(next, next2));
					input.get(next);
				}
			} break;
			default: {
				if (next >= '0' && next <= '7') {
					int result = Config::parseHex1(next);
					input.get(next);
					if (next >= '0' && next <= '7') {
						result = (result << 3) | Config::parseHex1(next);
						input.get(next);
						if (next >= '0' && next <= '7') {
							result = (result << 3) | Config::parseHex1(next);
							input.get(next);
						}
					}
					output.push_back(result);
				} else {
					std::cerr << "Caractère d'échappement `" << next << "` ("
							  << ((int)(unsigned char)next) << ") invalide"
							  << std::endl;
					exit(1);
				}
			}
			}
		} else {
			output.push_back(next);
			input.get(next);
		}
	}
}
