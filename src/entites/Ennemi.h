#ifndef _ENNEMI_H
#define _ENNEMI_H

#include "Entite.h"
#include <istream>

/**
 * @brief Un ennemi.
 */
class Ennemi : public Entite {
public:
	/**
	 * @brief Construit un ennemi selon des données.
	 */
	Ennemi(std::istream& data);

	/**
	 * @brief Retourne la portee de l'ennemi.
	 * @return La portée de l'ennemi.
	 */
	int getPortee() const;

	/**
	 * @brief Retourne le type de l'ennemi.
	 * @return Le type de l'ennemi.
	 */
	int getType() const;

protected:
	/**
	 * @brief La portée de l'ennemi.
	 */
	int portee;

	/**
	 * @brief Le type d'IA utilisée par l'ennemi.
	 */
	int type;
};

#endif
