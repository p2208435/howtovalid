#include "EnnemiMalin.h"

EnnemiMalin::EnnemiMalin(std::istream& data) : Ennemi(data) {
	this->degats = 1;
	this->texture = 20;
	this->portee = 3;
	this->offSetTexture = 18;
	this->eviteTuiles.insert('!');
	this->eviteTuiles.insert('/');
}
