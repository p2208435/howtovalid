#ifndef _ENNEMI_MALIN_H
#define _ENNEMI_MALIN_H

#include "Ennemi.h"
#include <istream>

/**
 * @brief Un ennemi malin.
 */
class EnnemiMalin : public Ennemi {
public:
	/**
	 * @brief Construit un ennemi malin selon des données.
	 */
	EnnemiMalin(std::istream& data);
};

#endif
