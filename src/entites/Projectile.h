#ifndef _PROJECTILE_H
#define _PROJECTILE_H

#include "Entite.h"

/**
 * @brief Un projectile.
 */
class Projectile : public Entite {
public:
	/**
	 * @brief Construit un projectile.
	 * @param x Sa position x.
	 * @param y Sa position y.
	 * @param angle Son angle.
	 * @param vitesse Sa vitesse.
	 * @param degats Ses dégâts.
	 */
	Projectile(double x, double y, double angle, double vitesse,
	           unsigned int degats);

	/**
	 * @brief Fait avancer le projectile.
	 */
	void avancer();

	/**
	 * @brief Retourne l'angle du projectile.
	 * @return L'angle du projectile.
	 */
	double getAngle() const;

private:
	/**
	 * @brief L'angle du projectile.
	 */
	double angle;
};

#endif
