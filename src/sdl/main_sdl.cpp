#include "../partie/Config.h"
#include "../partie/Partie.h"
#include "sdlWin.h"

int main(int argc, char** argv) {
	Config config("./data/data.txt");
	Partie partie(&config);
	SdlWin win(&partie, &config);
	win.boucle(100);
	return 0;
}
