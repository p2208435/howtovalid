#ifndef _SDLWIN_H
#define _SDLWIN_H

#include "../partie/Config.h"
#include "../partie/Partie.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <string>
#include <unordered_set>
#include <vector>

/**
 * @brief Un environnement graphique pour l'affichage d'une partie.
 */
class SdlWin {
public:
	/**
	 * @brief Crée un environnement graphique pour l'affichage d'une partie.
	 * @param partie La partie à afficher.
	 * @param config La configuration de l'affichage.
	 */
	SdlWin(Partie* partie, const Config* config);

	/**
	 * @brief Détruit l'environnement graphique.
	 */
	~SdlWin();

	/**
	 * @brief Lance la boucle de jeu dans l'environnement graphique.
	 * @param mspt Le nombre de millisecondes pris par un tour de boucle.
	 */
	void boucle(unsigned int mspt);

private:
	// Partie et configuration

	/**
	 * @brief La partie gérée par l'environnement graphique.
	 */
	Partie* partie;

	/**
	 * @brief La configuration de l'environnement graphique.
	 */
	const Config* config;

	// Boucle de jeu

	/**
	 * @brief La liste des touches appuyées à un moment donné.
	 */
	std::unordered_set<SDL_Keycode> touches;

	// Contexte d'affichage

	/**
	 * @brief La fenêtre ouverte par l'environnement.
	 */
	SDL_Window* window;

	/**
	 * @brief Le contexte de rendu de l'environnement.
	 */
	SDL_Renderer* renderer;

	// Police du texte

	/**
	 * @brief La taille de la police.
	 */
	int taillePolice;

	/**
	 * @brief La hauteur d'une ligne de texte.
	 */
	int hauteurLignePolice;

	/**
	 * @brief La police du texte.
	 */
	TTF_Font* police;

	// Textures de tuiles

	/**
	 * @brief La largeur d'une tuile en pixels.
	 */
	int largeurTuile;

	/**
	 * @brief La hauteur d'une tuile en pixels.
	 */
	int hauteurTuile;

	/**
	 * @brief La texture des tuiles.
	 */
	SDL_Texture* textureTuiles;

	/**
	 * @brief La texture du joueur.
	 */
	SDL_Texture* textureJoueur;

	/**
	 * @brief La texture des ennemis.
	 */
	SDL_Texture* textureEnnemis;

	/**
	 * @brief Un tableau contenant les textures: Tuiles - Joueur - Ennemis.
	 */
	SDL_Texture* textures[3];

	// Textures d'interface

	/**
	 * @brief La largeur de la fenêtre en pixels.
	 */
	int largeurFenetre;

	/**
	 * @brief La hauteur de la fenêtre en pixels.
	 */
	int hauteurFenetre;

	/**
	 * @brief La hauteur du bandeau du haut en pixels.
	 */
	int hauteurBandeauHaut;

	/**
	 * @brief La hauteur du bandeau du bas en pixels.
	 */
	int hauteurBandeauBas;

	/**
	 * @brief La hauteur visible de la carte en pixels.
	 */
	int hauteurCarte;

	/**
	 * @brief La texture de l'arrière-plan du menu.
	 */
	SDL_Texture* arrierePlanMenu;

	/**
	 * @brief La texture du titre du jeu.
	 */
	SDL_Texture* imageTitre;

	/**
	 * @brief La texture des boutons de sélection du personnage.
	 */
	SDL_Texture* boutonsPersonnage;

	/**
	 * @brief La texture de l'écran de victoire.
	 */
	SDL_Texture* ecranVictoire;

	/**
	 * @brief La texture de l'écran de transition entre deux niveaux.
	 */
	SDL_Texture* ecranSuivant;

	/**
	 * @brief La texture de l'écran de défaite.
	 */
	SDL_Texture* ecranDefaite;

	/**
	 * @brief La texture d'arrière-plan des bandeaux.
	 */
	SDL_Texture* arrierePlanBandeaux;

	// Autres membres

	/**
	 * @brief Compteur global pour des animations.
	 */
	int varAnim;

	// Gestion des appuis de touches

	/**
	 * @brief Définit l'état de l'appui d'une touche.
	 * @param touche L'identifiant de la touche.
	 * @param appui L'état de la touche.
	 */
	void appuiTouche(SDL_Keycode touche, bool appui);

	/**
	 * @brief Envoie à la partie les appuis sur les touches.
	 */
	void activeTouches();

	// Affichage des différents écrans

	/**
	 * @brief Affiche l'écran titre.
	 */
	void afficherEcranTitre() const;

	/**
	 * @brief Affiche l'écran de victoire.
	 */
	void afficherEcranVictoire() const;

	/**
	 * @brief Affiche l'écran de transition entre deux niveaux.
	 */
	void afficherEcranSuivant() const;

	/**
	 * @brief Affiche l'écran de défaite.
	 */
	void afficherEcranDefaite() const;

	/**
	 * @brief Affiche la carte du jeu.
	 */
	void afficherCarte() const;

	/**
	 * @brief Affiche une boîte de dialogue.
	 * @param texte Le texte à afficher.
	 * @param fond Si il faut réafficher le bandeau.
	 */
	void afficherDialogue(const std::vector<std::string>& texte,
	                      bool fond) const;

	/**
	 * @brief Affiche l'interface du jeu.
	 */
	void afficherInterface() const;

	// Affichage des textures génériques

	/**
	 * @brief Récupère la taille d'une texture dans un SDL_Point.
	 * @param texture La texture dont on souhaite connaître la taille.
	 * @return Un SDL_Point qui contient la taille (largeur, hauteur) de la texture.
	 */
	SDL_Point getTailleTexture(SDL_Texture* texture) const;

	/**
	 * @brief Affiche une texture.
	 * @param texture Texture à placer.
	 * @param destX Position X où la texture sera placée.
	 * @param destY Position Y où la texture sera placée.
	 */
	void afficherTexture(SDL_Texture* texture, double destX,
	                     double destY) const;

	/**
	 * @brief Affiche une texture.
	 * @param texture Texture à placer.
	 * @param destX Position X où la texture sera placée.
	 * @param destY Position Y où la texture sera placée.
	 * @param largeurDest Largeur de la texture.
	 * @param hauteurDest Hauteur de la texture.
	 * @param sourceX Position X de la texture à utiliser sur la source. 0 par defaut.
	 * @param sourceY Position Y de la texture à utiliser sur la source. 0 par defaut.
	 * @param largeurSource Largeur de la texture à utiliser sur la source. 0 par defaut -> texture entière.
	 * @param hauteurSource Hauteur de la texture à utiliser sur la source.	0 par defaut -> texture entière.
	 */
	void afficherTexture(SDL_Texture* texture, double destX, double destY,
	                     int largeurDest, int hauteurDest, int sourceX = 0,
	                     int sourceY = 0, int largeurSource = 0,
	                     int hauteurSource = 0) const;

	// Affichage des tuiles

	/**
	 * @brief Affiche une tuile.
	 * @param sourceX Position X de la tuile sur la source.
	 * @param sourceY Position Y de la tuile sur la source.
	 * @param destX Position X où la tuile sera placée.
	 * @param destY Position Y où la tuile sera placée.
	 * @param iT Indice de la texture de la tuile.
	 */
	void afficherTuile(int sourceX, int sourceY, double destX, double destY,
	                   int iT) const;

	/**
	 * @brief Affiche une tuile avec une rotation.
	 * @param sourceX Position X de la tuile sur la source.
	 * @param sourceY Position Y de la tuile sur la source.
	 * @param destX Position X où la tuile sera placée.
	 * @param destY Position Y où la tuile sera placée.
	 * @param angle Angle de la tuile.
	 * @param iT Indice de la texture de la tuile.
	 */
	void afficherTuileEx(int sourceX, int sourceY, double destX, double destY,
	                     double angle, int iT) const;

	// Affichage du texte

	/**
	 * @brief Affiche du texte aligné dans une boîte.
	 * @param texte Le texte à afficher.
	 * @param couleur Couleur du texte.
	 * @param x Gauche de la boîte.
	 * @param y Haut de la boîte.
	 * @param w Largeur de la boîte.
	 * @param h Hauteur de la boîte.
	 * @param alignX Alignement horizontal: `ALIGN_START`/`ALIGN_START_PAD`/`ALIGN_CENTER`/`ALIGN_END_PAD`/`ALIGN_END`.
	 * @param alignY Alignement vertical: `ALIGN_START`/`ALIGN_CENTER`/`ALIGN_END`.
	 * @param taille Facteur de taille du texte.
	 */
	void afficherTexte(const char* texte, const SDL_Color& couleur, double x,
	                   double y, double w, double h, int alignX, int alignY,
	                   double taille) const;

	/**
	 * @brief Affiche du texte aligné dans une boîte.
	 * @param texte Le texte à afficher.
	 * @param couleur Couleur du texte.
	 * @param rect La boîte.
	 * @param alignX Alignement horizontal: `ALIGN_START`/`ALIGN_START_PAD`/`ALIGN_CENTER`/`ALIGN_END_PAD`/`ALIGN_END`.
	 * @param alignY Alignement vertical: `ALIGN_START`/`ALIGN_CENTER`/`ALIGN_END`.
	 * @param taille Facteur de taille du texte.
	 */
	void afficherTexte(const char* texte, const SDL_Color& couleur,
	                   const SDL_Rect& rect, int alignX, int alignY,
	                   double taille) const;

	/**
	 * @brief Affiche du texte aligné dans une boîte.
	 * @param texte Le texte à afficher.
	 * @param couleur Couleur du texte.
	 * @param x Gauche de la boîte.
	 * @param y Haut de la boîte.
	 * @param w Largeur de la boîte.
	 * @param h Hauteur de la boîte.
	 * @param alignX Alignement horizontal: `ALIGN_START`/`ALIGN_START_PAD`/`ALIGN_CENTER`/`ALIGN_END_PAD`/`ALIGN_END`.
	 * @param alignY Alignement vertical: `ALIGN_START`/`ALIGN_CENTER`/`ALIGN_END`.
	 * @param taille Facteur de taille du texte.
	 */
	void afficherTexte(const std::string& texte, const SDL_Color& couleur,
	                   double x, double y, double w, double h, int alignX,
	                   int alignY, double taille) const;

	/**
	 * @brief Affiche du texte aligné dans une boîte.
	 * @param texte Le texte à afficher.
	 * @param couleur Couleur du texte.
	 * @param rect La boîte.
	 * @param alignX Alignement horizontal: `ALIGN_START`/`ALIGN_START_PAD`/`ALIGN_CENTER`/`ALIGN_END_PAD`/`ALIGN_END`.
	 * @param alignY Alignement vertical: `ALIGN_START`/`ALIGN_CENTER`/`ALIGN_END`.
	 * @param taille Facteur de taille du texte.
	 */
	void afficherTexte(const std::string& texte, const SDL_Color& couleur,
	                   const SDL_Rect& rect, int alignX, int alignY,
	                   double taille) const;

	/**
	 * @brief Affiche du texte aligné dans une boîte.
	 * @param texte Le texte à afficher.
	 * @param couleur Couleur du texte.
	 * @param x Gauche de la boîte.
	 * @param y Haut de la boîte.
	 * @param w Largeur de la boîte.
	 * @param h Hauteur de la boîte.
	 * @param alignX Alignement horizontal: `ALIGN_START`/`ALIGN_CENTER`/`ALIGN_END`.
	 * @param alignY Alignement vertical: `ALIGN_START`/`ALIGN_CENTER`/`ALIGN_END`.
	 * @param taille Facteur de taille du texte.
	 * @param interligne Facteur de l'interligne.
	 */
	void afficherTexte(const std::vector<std::string>& texte,
	                   const SDL_Color& couleur, double x, double y, double w,
	                   double h, int alignX, int alignY, double taille,
	                   double interligne) const;

	/**
	 * @brief Affiche du texte aligné dans une boîte.
	 * @param texte Le texte à afficher.
	 * @param couleur Couleur du texte.
	 * @param rect La boîte.
	 * @param alignX Alignement horizontal: `ALIGN_START`/`ALIGN_CENTER`/`ALIGN_END`.
	 * @param alignY Alignement vertical: `ALIGN_START`/`ALIGN_CENTER`/`ALIGN_END`.
	 * @param taille Facteur de taille du texte.
	 * @param interligne Facteur de l'interligne.
	 */
	void afficherTexte(const std::vector<std::string>& texte,
	                   const SDL_Color& couleur, const SDL_Rect& rect,
	                   int alignX, int alignY, double taille,
	                   double interligne) const;

	// Constantes pour l'alignement du texte

	/**
	 * @brief Alignement au début de la boîte.
	 */
	constexpr static const int ALIGN_START = 0;

	/**
	 * @brief Alignement à la fin de la boîte.
	 */
	constexpr static const int ALIGN_END = 1;

	/**
	 * @brief Alignement au centre de la boîte.
	 */
	constexpr static const int ALIGN_CENTER = 2;

	/**
	 * @brief Alignement au début de la boîte, en laissant de la place pour avoir un espacement constant si l'autre direction est centrée.
	 */
	constexpr static const int ALIGN_START_PAD = 3;

	/**
	 * @brief Alignement à la fin de la boîte, en laissant de la place pour avoir un espacement constant si l'autre direction est centrée.
	 */
	constexpr static const int ALIGN_END_PAD = 4;
};

#endif
