#include "sdlWin.h"
#include "../objets/Objet.h"
#include <cmath>
#include <iostream>

#ifdef _WIN32
#include <windows.h>
#else
#include <sys/time.h>
#include <unistd.h>
#endif

// #define AFFICHE_TEXTE_COMME_BOITES

SdlWin::SdlWin(Partie* partie, const Config* config) {
	// Partie et configuration

	this->partie = partie;
	this->config = config;

	// Boucle de jeu

	this->touches.clear();

	// Contexte d'affichage

	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		std::cerr << "[ERR] Unable to init SDL" << std::endl;
		exit(1);
	}

	this->window = SDL_CreateWindow(
		this->config->getString("interface.fenetre.titre").c_str(), 100, 100,
		this->config->getInt("interface.fenetre.largeur"),
		this->config->getInt("interface.fenetre.hauteur"), 0);
	this->renderer =
		SDL_CreateRenderer(this->window, -1, SDL_RENDERER_ACCELERATED);

	if (SDL_SetRenderDrawBlendMode(this->renderer, SDL_BLENDMODE_BLEND) != 0) {
		printf("Erreur lors du changement de mode de couleur : %s",
		       SDL_GetError());
		exit(4);
	}

	// Police du texte

	if (TTF_Init() == -1) {
		printf("TTF_Init: %s\n", TTF_GetError());
		exit(2);
	}

	this->taillePolice = this->config->getInt("police.taille");
	this->hauteurLignePolice = this->config->getInt("police.ligne");
	this->police = TTF_OpenFont(this->config->getPath("police.police").c_str(),
	                            this->taillePolice);

	if (!this->police) {
		printf("Erreur de création de la police : %s", TTF_GetError());
		exit(3);
	}

	// Textures de tuiles

	this->largeurTuile = this->config->getInt("texture.largeur");
	this->hauteurTuile = this->config->getInt("texture.hauteur");
	this->textureTuiles = IMG_LoadTexture(
		this->renderer, this->config->getPath("texture.tuiles").c_str());
	this->textureJoueur = IMG_LoadTexture(
		this->renderer, this->config->getPath("texture.joueur").c_str());
	this->textureEnnemis = IMG_LoadTexture(
		this->renderer, this->config->getPath("texture.ennemis").c_str());
	this->textures[0] = this->textureTuiles;
	this->textures[1] = this->textureJoueur;
	this->textures[2] = this->textureEnnemis;

	// Textures d'interface

	this->largeurFenetre = this->config->getInt("interface.fenetre.largeur");
	this->hauteurFenetre = this->config->getInt("interface.fenetre.hauteur");
	this->hauteurBandeauHaut =
		this->config->getInt("interface.bandeauHaut.hauteur");
	this->hauteurBandeauBas =
		this->config->getInt("interface.bandeauBas.hauteur");
	this->hauteurCarte = this->hauteurFenetre - this->hauteurBandeauHaut -
						 this->hauteurBandeauBas;
	this->arrierePlanMenu = IMG_LoadTexture(
		this->renderer, this->config->getPath("image.fond").c_str());
	this->imageTitre = IMG_LoadTexture(
		this->renderer, this->config->getPath("image.titre").c_str());
	this->boutonsPersonnage = IMG_LoadTexture(
		this->renderer, this->config->getPath("image.personnages").c_str());
	this->ecranVictoire = IMG_LoadTexture(
		this->renderer, this->config->getPath("image.victoire").c_str());
	this->ecranSuivant = IMG_LoadTexture(
		this->renderer, this->config->getPath("image.suivant").c_str());
	this->ecranDefaite = IMG_LoadTexture(
		this->renderer, this->config->getPath("image.defaite").c_str());
	this->arrierePlanBandeaux = IMG_LoadTexture(
		this->renderer, this->config->getPath("image.bandeau").c_str());

	// Autres membres

	this->varAnim = -1;
}

SdlWin::~SdlWin() {
	TTF_CloseFont(this->police);
	TTF_Quit();
	SDL_DestroyRenderer(this->renderer);
	SDL_DestroyWindow(this->window);
	SDL_Quit();
}

void SdlWin::boucle(unsigned int mspt) {
	// Remise à zero de la boucle
	SDL_Event e;
	bool ok = true;
#ifndef _WIN32
	timeval st, et;
	gettimeofday(&st, nullptr);
#endif
	this->touches.clear();

	// Boucle de jeu
	while (true) {
		SDL_RenderClear(this->renderer);
		if (!this->partie->getPartieEnCours()) {
			// Écran titre
			this->afficherEcranTitre();
		} else if (this->partie->fini()) {
			if (this->partie->gagne()) {
				// Écran de victoire
				this->afficherEcranVictoire();
				if (this->varAnim >= 0)
					this->varAnim++;
				if (this->varAnim >= 23) {
					this->varAnim = -1;
					this->partie->actionClavier('p');
				}
			} else if (this->partie->getConstJoueur().gagne) {
				// Écran de transition
				this->afficherEcranSuivant();
			} else {
				// Écran de défaite
				this->afficherEcranDefaite();
			}
		} else {
			// Écran principal
			this->afficherCarte();
			this->afficherInterface();

			// Actions automatiques du jeu
			this->partie->actionsAutomatiques();
		}

		// Attente
		SDL_RenderPresent(this->renderer);
#ifdef _WIN32
		Sleep(mspt);
#else
		gettimeofday(&et, nullptr);
		int elapsed =
			(et.tv_sec - st.tv_sec) * 1000000ll + (et.tv_usec - st.tv_usec);
		int waittime = int(mspt) * 1000 - elapsed;
		usleep(fmax(1, waittime));
		gettimeofday(&st, nullptr);
#endif

		// Gestion des touches
		while (SDL_PollEvent(&e)) {
			if (e.type == SDL_QUIT) {
				ok = false;
				break;
			} else if (e.type == SDL_KEYUP && e.key.keysym.sym == SDLK_ESCAPE) {
				ok = false;
				break;
			} else if (e.type == SDL_KEYDOWN || e.type == SDL_KEYUP) {
				this->appuiTouche(e.key.keysym.sym, e.type == SDL_KEYDOWN);
			}
		}
		if (!ok)
			break;
		this->activeTouches();
	}
}

// Gestion des appuis de touches

void SdlWin::appuiTouche(SDL_Keycode touche, bool appui) {
	if (appui)
		this->touches.insert(touche);
	else
		this->touches.erase(touche);
}

void SdlWin::activeTouches() {
	if (this->varAnim != -1)
		return;
	if (this->touches.contains(SDLK_RETURN)) {
		if (this->partie->getPartieEnCours() && this->partie->fini() &&
		    this->partie->gagne())
			this->varAnim = 0;
		else
			this->partie->actionClavier('p');
	}
	if (this->touches.contains(SDLK_LSHIFT))
		this->partie->actionClavier('a');
	if (this->touches.contains(SDLK_z))
		this->partie->actionClavier('z');
	if (this->touches.contains(SDLK_q))
		this->partie->actionClavier('q');
	if (this->touches.contains(SDLK_s))
		this->partie->actionClavier('s');
	if (this->touches.contains(SDLK_d))
		this->partie->actionClavier('d');
	if (this->touches.contains(SDLK_o))
		this->partie->actionClavier('o');
	if (this->touches.contains(SDLK_k))
		this->partie->actionClavier('k');
	if (this->touches.contains(SDLK_l))
		this->partie->actionClavier('l');
	if (this->touches.contains(SDLK_m))
		this->partie->actionClavier('m');
	if (this->touches.contains(SDLK_SPACE))
		this->partie->actionClavier(' ');
}

// Affichage des différents écrans

void SdlWin::afficherEcranTitre() const {
	// Affichage de l'arrière plan
	SDL_RenderCopy(this->renderer, this->arrierePlanMenu, nullptr, nullptr);

	// Affichage du titre
	SDL_Point tailleTitre = this->getTailleTexture(this->imageTitre);
	int largeurAffichageTitre =
		(this->largeurFenetre / tailleTitre.x) * tailleTitre.x;
	int hauteurAffichageTitre =
		(this->largeurFenetre / tailleTitre.x) * tailleTitre.y;
	this->afficherTexture(this->imageTitre,
	                      (this->largeurFenetre - largeurAffichageTitre) / 2,
	                      (this->hauteurFenetre - hauteurAffichageTitre) / 16,
	                      largeurAffichageTitre, hauteurAffichageTitre);

	// Affichage des boutons de sélection du personnage
	SDL_Point tailleBoutonsPersonnage =
		this->getTailleTexture(this->boutonsPersonnage);
	int tailleAffichageBoutonsPersonnage =
		((this->largeurFenetre / tailleBoutonsPersonnage.y) *
	     tailleBoutonsPersonnage.y) /
		6;
	// Bouton facile
	this->afficherTexture(
		this->boutonsPersonnage,
		((this->largeurFenetre - (tailleAffichageBoutonsPersonnage)) / 5),
		((this->hauteurFenetre - tailleAffichageBoutonsPersonnage) / 16) * 7,
		tailleAffichageBoutonsPersonnage, tailleAffichageBoutonsPersonnage,
		tailleBoutonsPersonnage.y *
			(this->partie->getDifficulte() == 0 ? 0 : 1),
		0, tailleBoutonsPersonnage.y, tailleBoutonsPersonnage.y);
	// Bouton normal
	this->afficherTexture(
		this->boutonsPersonnage,
		((this->largeurFenetre - (tailleAffichageBoutonsPersonnage)) / 5) * 2.5,
		((this->hauteurFenetre - tailleAffichageBoutonsPersonnage) / 16) * 7,
		tailleAffichageBoutonsPersonnage, tailleAffichageBoutonsPersonnage,
		tailleBoutonsPersonnage.y *
			(this->partie->getDifficulte() == 1 ? 2 : 3),
		0, tailleBoutonsPersonnage.y, tailleBoutonsPersonnage.y);
	// Bouton difficile
	this->afficherTexture(
		this->boutonsPersonnage,
		((this->largeurFenetre - (tailleAffichageBoutonsPersonnage)) / 5) * 4,
		((this->hauteurFenetre - tailleAffichageBoutonsPersonnage) / 16) * 7,
		tailleAffichageBoutonsPersonnage, tailleAffichageBoutonsPersonnage,
		tailleBoutonsPersonnage.y *
			(this->partie->getDifficulte() == 2 ? 4 : 5),
		0, tailleBoutonsPersonnage.y, tailleBoutonsPersonnage.y);

	// Affichage du texte
	SDL_Color TextColor;
	TextColor.r = 50;
	TextColor.g = 50;
	TextColor.b = 50;
	TextColor.a = 255;

	// Texte choix personnage
	this->afficherTexte(
		this->config->getText("texte.choixPersonnage.texte"), TextColor,
		this->largeurFenetre * 0.5, this->hauteurFenetre * 0.28125, 0, 0,
		SdlWin::ALIGN_CENTER, SdlWin::ALIGN_START,
		this->config->getDouble("texte.choixPersonnage.taille"),
		this->config->getDouble("texte.choixPersonnage.interligne"));

	// Texte touches difficulté
	this->afficherTexte(
		this->config->getText("texte.touchesDifficulte.texte"), TextColor,
		this->largeurFenetre * 0.5, this->hauteurFenetre * 0.335, 0, 0,
		SdlWin::ALIGN_CENTER, SdlWin::ALIGN_START,
		this->config->getDouble("texte.touchesDifficulte.taille"),
		this->config->getDouble("texte.touchesDifficulte.interligne"));

	// Texte nom du personnage
	this->afficherTexte(
		this->config->getText(std::string("texte.personnage.nom.") +
	                          std::to_string(this->partie->getDifficulte())),
		TextColor, this->largeurFenetre * 0.5, this->hauteurFenetre * 0.5625, 0,
		0, SdlWin::ALIGN_CENTER, SdlWin::ALIGN_CENTER,
		this->config->getDouble("texte.personnage.nom.taille"),
		this->config->getDouble("texte.personnage.nom.interligne"));

	// Texte description du personnage
	this->afficherTexte(
		this->config->getText(std::string("texte.personnage.description.") +
	                          std::to_string(this->partie->getDifficulte())),
		TextColor, this->largeurFenetre * 0.5, this->hauteurFenetre * 0.625, 0,
		0, SdlWin::ALIGN_CENTER, SdlWin::ALIGN_START,
		this->config->getDouble("texte.personnage.description.taille"),
		this->config->getDouble("texte.personnage.description.interligne"));

	// Texte commencer
	this->afficherTexte(this->config->getText("texte.commencer.texte"),
	                    TextColor, this->largeurFenetre * 0.5,
	                    this->hauteurFenetre * 0.8, 0, 0, SdlWin::ALIGN_CENTER,
	                    SdlWin::ALIGN_START,
	                    this->config->getDouble("texte.commencer.taille"),
	                    this->config->getDouble("texte.commencer.interligne"));
}

void SdlWin::afficherEcranVictoire() const {
	// Animation de victoire
	int anim = fmax(0, this->varAnim);
	anim = anim - fmax(0, fmin(3, anim - 2));
	anim = anim - fmax(0, fmin(10, anim - 5));
	int texLargeur, texHauteur;
	SDL_QueryTexture(this->ecranVictoire, nullptr, nullptr, &texLargeur,
	                 &texHauteur);
	int imgLargeur = texLargeur / 10;
	this->afficherTexture(this->ecranVictoire, 0, 0, this->largeurFenetre,
	                      this->hauteurFenetre, imgLargeur * anim, 0,
	                      imgLargeur, texHauteur);
	if (this->varAnim != -1)
		return;

	// Affichage du texte
	SDL_Color TextColor;
	TextColor.r = 50;
	TextColor.g = 50;
	TextColor.b = 50;
	TextColor.a = 255;

	// Texte continuer
	this->afficherTexte(this->config->getText("texte.continuer.texte"),
	                    TextColor, this->largeurFenetre * 0.7,
	                    this->hauteurFenetre * 0.9, 0, 0, SdlWin::ALIGN_CENTER,
	                    SdlWin::ALIGN_START,
	                    this->config->getDouble("texte.continuer.taille"),
	                    this->config->getDouble("texte.continuer.interligne"));
}

void SdlWin::afficherEcranSuivant() const {
	this->afficherTexture(this->ecranSuivant, 0, 0, this->largeurFenetre,
	                      this->hauteurFenetre);

	// Affichage du texte
	SDL_Color TextColor;
	TextColor.r = 50;
	TextColor.g = 50;
	TextColor.b = 50;
	TextColor.a = 255;

	// Texte continuer
	this->afficherTexte(this->config->getText("texte.continuer.texte"),
	                    TextColor, this->largeurFenetre * 0.5,
	                    this->hauteurFenetre * 0.8, 0, 0, SdlWin::ALIGN_CENTER,
	                    SdlWin::ALIGN_START,
	                    this->config->getDouble("texte.continuer.taille"),
	                    this->config->getDouble("texte.continuer.interligne"));
}

void SdlWin::afficherEcranDefaite() const {
	this->afficherTexture(this->ecranDefaite, 0, 0, this->largeurFenetre,
	                      this->hauteurFenetre,
	                      this->partie->getDifficulte() * 160, 0, 160, 260);

	// Affichage du texte
	SDL_Color TextColor;
	TextColor.r = 255;
	TextColor.g = 255;
	TextColor.b = 255;
	TextColor.a = 255;

	// Texte continuer
	this->afficherTexte(this->config->getText("texte.continuer.texte"),
	                    TextColor, this->largeurFenetre * 0.5,
	                    this->hauteurFenetre * 0.9, 0, 0, SdlWin::ALIGN_CENTER,
	                    SdlWin::ALIGN_START,
	                    this->config->getDouble("texte.continuer.taille"),
	                    this->config->getDouble("texte.continuer.interligne"));
}

void SdlWin::afficherCarte() const {
	// Calcul des variables liées à l'affichage
	const Joueur& joueur = this->partie->getConstJoueur();
	const Carte& carte = this->partie->getConstCarte();
	double offW = double(this->largeurFenetre) / this->largeurTuile,
		   offH = double(this->hauteurCarte) / this->hauteurTuile,
		   offC = double(this->hauteurBandeauHaut) / this->hauteurTuile;
	double offX = fmax(0.0, fmin(carte.getLargeur() - offW,
	                             joueur.getPositionX() - offW / 2.0)),
		   offY = fmax(0.0, fmin((carte.getHauteur()) - offH,
	                             joueur.getPositionY() - offH / 2.0));
	int texLargeur;
	SDL_QueryTexture(this->textureTuiles, nullptr, nullptr, &texLargeur,
	                 nullptr);
	int nbTuileX = texLargeur / this->largeurTuile;
	unsigned int maxX = ceil(fmin(offX + offW, carte.getLargeur())),
				 maxY = ceil(fmin(offY + offH, carte.getHauteur()));

	// Affichage des tuiles
	SDL_SetTextureColorMod(this->textures[0], 255, 255, 255);
	for (unsigned int y = offY; y < maxY; y++) {
		for (unsigned int x = offX; x < maxX; x++) {
			unsigned int textureId = carte.getTexture(x, y);
			this->afficherTuile(textureId % nbTuileX, textureId / nbTuileX,
			                    x - offX, y - offY + offC, 0);
		}
	}

	// Affichage du joueur
	if (joueur.getTempsDepuisDegats() == 0)
		SDL_SetTextureColorMod(this->textures[1], 255, 0, 0);
	else
		SDL_SetTextureColorMod(this->textures[1], 255, 255, 255);
	this->afficherTuile(joueur.texture % 6, joueur.texture / 6,
	                    joueur.getPositionX() - offX,
	                    joueur.getPositionY() - offY + offC, 1);

	// Affichage des ennemis
	for (const auto& ennemi : this->partie->getConstEnnemis()) {
		double ex = ennemi->getPositionX() - offX,
			   ey = ennemi->getPositionY() - offY;
		if (ex >= -1 && ex < offW && ey >= -1 && ey < offH) {
			if (ennemi->getTempsDepuisDegats() == 0)
				SDL_SetTextureColorMod(this->textures[2], 255, 0, 0);
			else
				SDL_SetTextureColorMod(this->textures[2], 255, 255, 255);
			this->afficherTuile(ennemi->texture % 6, ennemi->texture / 6, ex,
			                    ey + offC, 2);
		}
	}

	// Affichage des projectiles
	for (const auto& projectile : this->partie->getConstProjectiles()) {
		double px = projectile->getPositionX() - offX,
			   py = projectile->getPositionY() - offY;
		if (px >= -1 && px < offW && py >= -1 && py < offH) {
			if (projectile->getTempsDepuisDegats() == 0)
				SDL_SetTextureColorMod(this->textures[0], 255, 0, 0);
			else
				SDL_SetTextureColorMod(this->textures[0], 255, 255, 255);
			this->afficherTuileEx(projectile->texture % nbTuileX,
			                      projectile->texture / nbTuileX, px, py + offC,
			                      projectile->getAngle(), 0);
		}
	}

	// Affichage des particules
	SDL_SetTextureColorMod(this->textures[0], 255, 255, 255);
	for (const auto& particule : this->partie->getConstParticules()) {
		double px = particule->getX() - offX, py = particule->getY() - offY;
		if (px >= -1 && px < offW && py >= -1 && py < offH) {
			this->afficherTuileEx(particule->getTexture() % nbTuileX,
			                      particule->getTexture() / nbTuileX, px,
			                      py + offC, particule->getAngle(), 0);
		}
	}
}

void SdlWin::afficherDialogue(const std::vector<std::string>& texte,
                              bool fond) const {
	// Affichage du bandeau
	if (fond) {
		SDL_Rect dst;
		dst.x = 0;
		dst.y = this->hauteurBandeauHaut + this->hauteurCarte;
		dst.w = this->largeurFenetre;
		dst.h = this->hauteurBandeauBas;
		SDL_RenderCopy(this->renderer, this->arrierePlanBandeaux, nullptr,
		               &dst);
	}

	// Affichage du texte
	SDL_Color color;
	color.r = 50;
	color.g = 50;
	color.b = 50;
	color.a = 255;
	this->afficherTexte(
		texte, color, 0, this->hauteurBandeauHaut + this->hauteurCarte,
		this->largeurFenetre, this->hauteurBandeauBas, SdlWin::ALIGN_CENTER,
		SdlWin::ALIGN_CENTER,
		this->config->getDouble("texte.interface.dialogue.taille"),
		this->config->getDouble("texte.interface.dialogue.interligne"));
}

void SdlWin::afficherInterface() const {
	const Joueur& joueur = this->partie->getConstJoueur();

	// Affichage des bandeaux
	SDL_Rect dest_arr;
	dest_arr.x = 0;
	dest_arr.y = 0;
	dest_arr.w = this->largeurFenetre;
	dest_arr.h = this->hauteurBandeauHaut;
	SDL_RenderCopy(this->renderer, this->arrierePlanBandeaux, nullptr,
	               &dest_arr);
	dest_arr.y = this->hauteurBandeauHaut + this->hauteurCarte;
	dest_arr.h = this->hauteurBandeauBas;
	SDL_RenderCopy(this->renderer, this->arrierePlanBandeaux, nullptr,
	               &dest_arr);

	// Affichage de la boîte de dialogue
	bool afficheInventaire = true;
	for (const auto& dia : this->partie->getConstDialogues()) {
		if (dia.x == (unsigned int)(joueur.getPositionX() + 0.5) &&
		    dia.y == (unsigned int)ceil(joueur.getPositionY())) {
			this->afficherDialogue(dia.texte, false);
			afficheInventaire = false;
			break;
		}
	}

	// Affichage de l'inventaire
	if (afficheInventaire) {
		int texLargeur;
		SDL_QueryTexture(this->textureTuiles, nullptr, nullptr, &texLargeur,
		                 nullptr);
		int nbTuileX = texLargeur / this->largeurTuile;
		const std::vector<Objet*> inv =
			this->partie->getConstJoueur().getConstInventaire();
		for (unsigned int i = 0; i < inv.size(); i++) {
			Objet* obj = inv[i];
			int tx = obj->texture % nbTuileX;
			int ty = obj->texture / nbTuileX;
			int off = (this->hauteurBandeauBas - this->largeurTuile) / 2;
			SDL_Rect src;
			src.x = tx * this->largeurTuile;
			src.y = ty * this->largeurTuile;
			src.w = this->largeurTuile;
			src.h = this->hauteurTuile;
			SDL_Rect dst;
			dst.x = this->largeurFenetre - 1 - (i + 1) * off -
					(i + 1) * this->largeurTuile;
			dst.y = this->hauteurBandeauHaut + this->hauteurCarte + off;
			dst.w = this->largeurTuile;
			dst.h = this->largeurTuile;
			SDL_RenderCopy(this->renderer, this->textureTuiles, &src, &dst);
		}
	}

	// Affichage des pv du joueur
	char txtPvJoueur[this->config->getInt("texte.interface.pvJoueur.longueur")];
	sprintf(
		txtPvJoueur, "%s%4.1f%s%4.1f%s",
		this->config->getString("texte.interface.pvJoueur.texteGauche").c_str(),
		(double)joueur.getPV() / 2.0,
		this->config->getString("texte.interface.pvJoueur.texteMilieu").c_str(),
		(double)joueur.getPVMax() / 2.0,
		this->config->getString("texte.interface.pvJoueur.texteDroite")
			.c_str());
	SDL_Color TextColor;
	TextColor.r = 50;
	TextColor.g = 50;
	TextColor.b = 50;
	TextColor.a = 255;
	this->afficherTexte(
		txtPvJoueur, TextColor, 0, 0, 0, this->hauteurBandeauHaut,
		SdlWin::ALIGN_START_PAD, SdlWin::ALIGN_CENTER,
		this->config->getDouble("texte.interface.pvJoueur.taille"));
}

// Affichage des textures génériques

SDL_Point SdlWin::getTailleTexture(SDL_Texture* texture) const {
	SDL_Point size;
	SDL_QueryTexture(texture, nullptr, nullptr, &size.x, &size.y);
	return size;
}

void SdlWin::afficherTexture(SDL_Texture* texture, double destX,
                             double destY) const {
	SDL_Point tailleTexture = this->getTailleTexture(texture);
	SDL_Rect source;
	source.x = 0;
	source.y = 0;
	source.w = tailleTexture.x;
	source.h = tailleTexture.y;
	SDL_Rect dest;
	dest.x = destX;
	dest.y = destY;
	dest.w = tailleTexture.x;
	dest.h = tailleTexture.y;
	SDL_RenderCopy(this->renderer, texture, &source, &dest);
}

void SdlWin::afficherTexture(SDL_Texture* texture, double destX, double destY,
                             int largeurDest, int hauteurDest, int sourceX,
                             int sourceY, int largeurSource,
                             int hauteurSource) const {
	SDL_Rect source;
	source.x = sourceX;
	source.y = sourceY;
	if (largeurSource == 0) {
		SDL_Point tailleTexture = this->getTailleTexture(texture);
		source.w = tailleTexture.x;
	} else {
		source.w = largeurSource;
	}
	if (hauteurSource == 0) {
		SDL_Point tailleTexture = this->getTailleTexture(texture);
		source.h = tailleTexture.y;
	} else {
		source.h = hauteurSource;
	}
	SDL_Rect dest;
	dest.x = destX;
	dest.y = destY;
	dest.w = largeurDest;
	dest.h = hauteurDest;
	SDL_RenderCopy(this->renderer, texture, &source, &dest);
}

// Affichage des tuiles

void SdlWin::afficherTuile(int sourceX, int sourceY, double destX, double destY,
                           int iT) const {
	SDL_Rect source;
	source.x = sourceX * this->largeurTuile;
	source.y = sourceY * this->hauteurTuile;
	source.w = this->largeurTuile;
	source.h = this->hauteurTuile;
	SDL_Rect dest;
	dest.x = destX * this->largeurTuile;
	dest.y = destY * this->hauteurTuile;
	dest.w = this->largeurTuile;
	dest.h = this->hauteurTuile;
	SDL_RenderCopy(this->renderer, this->textures[iT], &source, &dest);
}

void SdlWin::afficherTuileEx(int sourceX, int sourceY, double destX,
                             double destY, double angle, int iT) const {
	SDL_Rect source;
	source.x = sourceX * this->largeurTuile;
	source.y = sourceY * this->hauteurTuile;
	source.w = this->largeurTuile;
	source.h = this->hauteurTuile;
	SDL_Rect dest;
	dest.x = destX * this->largeurTuile;
	dest.y = destY * this->hauteurTuile;
	dest.w = this->largeurTuile;
	dest.h = this->hauteurTuile;
	SDL_RenderCopyEx(this->renderer, this->textures[iT], &source, &dest,
	                 angle * 180.0 / M_PI, nullptr, SDL_FLIP_NONE);
}

// Affichage du texte

void SdlWin::afficherTexte(const char* texte, const SDL_Color& couleur,
                           double x, double y, double w, double h, int alignX,
                           int alignY, double taille) const {
	// Rendu du texte sur une surface
	SDL_Surface* surface = TTF_RenderUTF8_Solid(this->police, texte, couleur);
	SDL_Rect dst;
	dst.w = surface->w * taille;
	dst.h = surface->h * taille;

	// Alignement horizontal
	switch (alignX) {
	case SdlWin::ALIGN_START:
		dst.x = x;
		break;
	case SdlWin::ALIGN_END:
		dst.x = x + w - surface->w * taille;
		break;
	case SdlWin::ALIGN_CENTER:
		dst.x = x + (w - surface->w * taille) / 2;
		break;
	case SdlWin::ALIGN_START_PAD:
		dst.x = x + (h - surface->h * taille) / 2;
		break;
	case SdlWin::ALIGN_END_PAD:
		dst.x = x + w - surface->w * taille - (h - surface->h * taille) / 2;
		break;
	}

	// Alignement vertical
	switch (alignY) {
	case SdlWin::ALIGN_START:
		dst.y = y;
		break;
	case SdlWin::ALIGN_END:
		dst.y = y + h - surface->h * taille;
		break;
	case SdlWin::ALIGN_CENTER:
		dst.y = y + (h - surface->h * taille) / 2;
		break;
	}

#ifndef AFFICHE_TEXTE_COMME_BOITES
	// Conversion de la surface en texture et affichage du texte
	SDL_Texture* texture =
		SDL_CreateTextureFromSurface(this->renderer, surface);
	SDL_RenderCopy(this->renderer, texture, nullptr, &dst);
	SDL_DestroyTexture(texture);
#else
	// Affichage d'un rectangle, log du texte dans la console
	printf(
		"\e[33m%3d %3d %3d %3d \e[35m%4d %4d \e[36m%4d %4d %4d %4d \e[34m%s\e[0m\n",
		couleur.r, couleur.g, couleur.b, couleur.a, surface->w, surface->h,
		dst.x, dst.y, dst.w, dst.h, texte);
	SDL_SetRenderDrawColor(this->renderer, couleur.r, couleur.g, couleur.b,
	                       couleur.a);
	SDL_RenderDrawRect(this->renderer, &dst);
#endif
	SDL_FreeSurface(surface);
}

void SdlWin::afficherTexte(const char* texte, const SDL_Color& couleur,
                           const SDL_Rect& rect, int alignX, int alignY,
                           double taille) const {
	this->afficherTexte(texte, couleur, rect.x, rect.y, rect.w, rect.h, alignX,
	                    alignY, taille);
}

void SdlWin::afficherTexte(const std::string& texte, const SDL_Color& couleur,
                           double x, double y, double w, double h, int alignX,
                           int alignY, double taille) const {
	this->afficherTexte(texte.c_str(), couleur, x, y, w, h, alignX, alignY,
	                    taille);
}

void SdlWin::afficherTexte(const std::string& texte, const SDL_Color& couleur,
                           const SDL_Rect& rect, int alignX, int alignY,
                           double taille) const {
	this->afficherTexte(texte.c_str(), couleur, rect.x, rect.y, rect.w, rect.h,
	                    alignX, alignY, taille);
}

void SdlWin::afficherTexte(const std::vector<std::string>& texte,
                           const SDL_Color& couleur, double x, double y,
                           double w, double h, int alignX, int alignY,
                           double taille, double interligne) const {
	// Alignement vertical
	double hauteurLigne = this->hauteurLignePolice * taille * interligne;
	double offY = 0;
	switch (alignY) {
	case SdlWin::ALIGN_START:
		offY = 0;
		break;
	case SdlWin::ALIGN_END:
		offY = h - (int(texte.size()) - 1) * hauteurLigne / 2;
		break;
	case SdlWin::ALIGN_CENTER:
		offY = (h - (int(texte.size()) - 1) * hauteurLigne) / 2;
		break;
	}

	// Affichage individuel des lignes de texte
	for (unsigned int i = 0; i < texte.size(); i++) {
		this->afficherTexte(texte[i].c_str(), couleur, x, y + offY, w, 0,
		                    alignX, alignY, taille);
		offY += hauteurLigne;
	}
}

void SdlWin::afficherTexte(const std::vector<std::string>& texte,
                           const SDL_Color& couleur, const SDL_Rect& rect,
                           int alignX, int alignY, double taille,
                           double interligne) const {
	this->afficherTexte(texte, couleur, rect.x, rect.y, rect.w, rect.h, alignX,
	                    alignY, taille, interligne);
}
