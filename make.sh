echo '
CC = g++
CCFLAGS = -Wall -std=c++20 -ggdb
INCLUDES = -I/usr/include/SDL2

LD = g++
LDFLAGS =
LIBS =

LIBS_TXT =
LIBS_SDL = -lSDL2 -lSDL2_image -lSDL2_ttf
LIBS_TEST =

all: make_dirs bin/HowToValid bin/HowToValid_console bin/HowToValid_tests

nothing:

docs:
	doxygen doc/Doxyfile

format:
	clang-format -i src/**/*

clean:
	rm -rf obj/* bin/* doc/html tmp/*

make_dirs:
	mkdir -p src obj bin data doc tmp' > Makefile

ls src/*/*.cpp | sed -E 's/src\/(.*)\/.*?\.cpp/obj\/\1/' | echo $(cat -) | sed -E 's/(obj\/\w+)( \1)+/\1/g' | sed -E 's/(.*)/\tmkdir -p \1/' >> Makefile
ls src/*/*.cpp | sed -E 's/src\/(.*)\.cpp/obj\/\1.o/' | echo $(cat -) | sed -E 's/obj\/txt\/\w*\.o//g' | sed -E 's/obj\/test\/\w*\.o//g' | sed -E 's/\s+/ /g' | sed -E 's/\s$//g' | sed -E 's/(.*)/bin\/HowToValid: \1\n\t$(LD) \1 -o bin\/HowToValid $(LDFLAGS) $(LIBS) $(LIBS_SDL)/' >> Makefile
ls src/*/*.cpp | sed -E 's/src\/(.*)\.cpp/obj\/\1.o/' | echo $(cat -) | sed -E 's/obj\/sdl\/\w*\.o//g' | sed -E 's/obj\/test\/\w*\.o//g' | sed -E 's/\s+/ /g' | sed -E 's/\s$//g' | sed -E 's/(.*)/bin\/HowToValid_console: \1\n\t$(LD) \1 -o bin\/HowToValid_console $(LDFLAGS) $(LIBS) $(LIBS_TXT)/' >> Makefile
ls src/*/*.cpp | sed -E 's/src\/(.*)\.cpp/obj\/\1.o/' | echo $(cat -) | sed -E 's/obj\/txt\/\w*\.o//g' | sed -E 's/obj\/sdl\/\w*\.o//g' | sed -E 's/\s+/ /g' | sed -E 's/\s$//g' | sed -E 's/(.*)/bin\/HowToValid_tests: \1\n\t$(LD) \1 -o bin\/HowToValid_tests $(LDFLAGS) $(LIBS) $(LIBS_TEST)/' >> Makefile
ls src/*/*.cpp | sed -E 's/src\/(.*)\.cpp/obj\/\1.o: src\/\1.cpp\n\t$(CC) -c $(CCFLAGS) $(INCLUDES) src\/\1.cpp -o obj\/\1.o/' >> Makefile

make $@
