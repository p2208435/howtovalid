+------------+
| HowToValid |
+------------+

Projet 28473, MODOT PIERRE p2208435, FONTAINE KYLIAN p2112105, DECHER JOLAN p2100764

Pour une description plus complète, voir README.md

==== DESCRIPTION ====

How To Valid est un jeu de style RPG dans lequel on incarne un étudiant dans une université dévastée par d'horribles monstres.
Les études étant malgré tout importantes, vous devrez quand même parcourir le campus et combattre les monstres pour valider votre année...
Attention à votre note et bonne chance !

== RÈGLES DU JEU ==

Évitez (ou vainquez) les monstres pour atteindre votre objectif (plaque dorée) sans que votre note tombe à 0/20 pour valider votre année.

== CONTRÔLES ==

Z/Q/S/D - Déplacement
O/K/L/M - Attaque
Shift Gauche - Marcher lentement
Espace - Utiliser un objet

==== FONCTIONNALITÉS ====

Plusieurs cartes à base de tuiles
Carte modifiable au cours du jeu
Différentes tuiles à effets variés
Objets utilisables
Différents ennemis à comportements variés
Projectiles
Animations et particules
Boîtes de dialogue
Système de configuration
Interfaces graphique et textuelle

==== COMPILATION ====

== Depuis un clone du projet ==

$ git clone https://forge.univ-lyon1.fr/p2208435/howtovalid.git
$ cd howtovalid
$ make

== Depuis une copie locale ==

Le Makefile est généré par un script et doit être regénéré après certaines actions
Avec node.js:
  Génération avec $ node ./make.js
  Compilation avec $ make [targets ...]
Avec bash:
  Génération et compilation avec $ ./make.sh [targets ...]

== Targets du Makefile ==

all        - make_dirs + bin/*
nothing    - ne fait rien
docs       - construit la doc avec doxygen
format     - formatte le code avec clang-format
clean      - supprime les fichiers générés
make_dirs  - crée les dossiers nécéssaires
bin/*      - lie les binaires
obj/**/*.o - compile les objets

==== EXÉCUTION ====

Les binaires doivent être exécutés depuis la racine du projet (cherchent des données dans ./data/)
Version graphique: bin/HowToValid
Version textuelle: bin/HowToValid_console
Tests d'intégrité: bin/HowToValid_tests

==== ORGANISATION DE L'ARCHIVE ====

src/*/        - sources C++
src/sdl/      - interface graphique
src/txt/      - interface textuelle
src/test/     - tests d'intégrité
obj/          - fichiers objets (généré)
bin/          - binaires (généré)
doc/Doxyfile  - configuration de doxygen
doc/html      - documentation au format HTML (généré)
data/         - données
tmp/          - fichiers temporaires
make.js       - générateur de Makefile (node.js)
make.sh       - générateur de Makefile (bash)
Makefile      - processus de compilation (make)
.clang-format - configuration de clang-format
